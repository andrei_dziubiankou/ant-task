/**
 * HelloWorld class.
 * @author Andrei Dziubiankou
 *
 */
public final class HelloWorld {

    private HelloWorld() {

    }

    /**
     * @param args for console
     */
    public static void main(final String[] args) {
        System.out.println("Hello World!!!!");
    }

}
