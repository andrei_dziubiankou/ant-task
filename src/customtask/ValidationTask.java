package customtask;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.Target;
import org.apache.tools.ant.Task;

/**
 * @author Andrei Dziubiankou
 *
 */
public class ValidationTask extends Task {
    private boolean checkDepends;
    private boolean checkDefaults;
    private boolean checkNames;
    private List<Filelocation> filelocations = new ArrayList<Filelocation>();

    /**
     * @param checkNames if target names not null, not empty and contains only letters A-z with '-'
     */
    public void setCheckNames(final boolean checkNames) {
        this.checkNames = checkNames;
    }

    /**
     * @param checkDepends if targets with depends are used only in one target
     */
    public void setCheckDepends(final boolean checkDepends) {
        this.checkDepends = checkDepends;
    }

    /**
     * @param checkDefaults if project contains default attribute
     */
    public void setCheckDefaults(final boolean checkDefaults) {
        this.checkDefaults = checkDefaults;
    }

    @Override
    public final void execute() throws BuildException {
        for (Filelocation filelocation:filelocations) {
            boolean hasDefaultTarget = true;
            boolean hasOneDepends = true;
            boolean isTargetNamesValid = true;
            String filename = filelocation.getFilename();
            System.out.println("start validation of file: " + filename);
            Project project = new Project();
            File buildFile = new File(filename);
            project.init();
            ProjectHelper.configureProject(project, buildFile);
            if (checkDefaults) {
                String defaultTarget = project.getDefaultTarget();
                if (defaultTarget == null) {
                    hasDefaultTarget = false;
                }
            }
            if (checkDepends || checkNames) {
                int countDepends = 0;
                Set<Map.Entry<String, Target>> targets = project.getTargets().entrySet();
                for (Entry<String, Target> target:targets) {
                    if (checkDepends && hasOneDepends) {
                        Enumeration<String> depends = target.getValue().getDependencies();
                        if (depends.hasMoreElements()) {
                            countDepends++;
                        }
                        if (countDepends > 1) {
                            hasOneDepends = false;
                        }
                    }
                    if (checkNames && isTargetNamesValid) {
                        String targetName = target.getKey();
                        Pattern p = Pattern.compile("[A-z-]*");
                        Matcher m = p.matcher(targetName);
                        if (!m.matches()) {
                            isTargetNamesValid = false;
                        }
                    }
                }
            }
            printResult(hasDefaultTarget, hasOneDepends, isTargetNamesValid);
        }
    }

    private void printResult(final boolean hasDefaultTarget, final boolean hasOneDepends,
                            final boolean isTargetNamesValid) {
        boolean validationResult = (hasDefaultTarget
                && hasOneDepends && isTargetNamesValid);
        if (!validationResult) {
            if (!hasDefaultTarget) {
                System.out.println("file don't have a default task");
            }
            if (!hasOneDepends) {
                System.out.println("file have depends not only in main task");
            }
            if (!isTargetNamesValid) {
                System.out.println("names of targets contains wrong characters");
            }
        } else {
            System.out.println("file is valid");
        }
    }

    /**
     * @return filelocation
     */
    public Filelocation createFilelocation() {
        Filelocation filelocation = new Filelocation();
        filelocations.add(filelocation);
        return filelocation;
    }

    /**
     * @author Andrei Dziubiankou
     *
     */
    public class Filelocation {

        /**
         * default constructor
         */
        public Filelocation() { }

        private String filename;

        /**
         * @param filename of buildfile
         */
        public void setFilename(final String filename) {
            this.filename = filename;
        }

        /**
         * @return filename of buildfile
         */
        public String getFilename() {
            return filename;
        }
    }
}
